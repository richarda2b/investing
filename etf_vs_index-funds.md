# ETF vs Index Funds

* Longer term:
  * 3-5+ years
  * Funds on shares, property
  * “Growth” investments
* Shorter term:
  * 1-3 years
  * Funds on cash, “fixed interest” (bonds)
  * “Income” investments
* “Distributions” 
  * Share of the income earned by a fund
  * Fund collects all the forms of income and profit made, and pay it out it to the unit holders as distributions
  * Can include
  * Dividends
  * Franking credits
  * Interest
  * Realised Capital Gains
  * Foreign income
* Dividends are subject to income tax
  * “Franking credits” means the company has already paid taxed for some portion of it which reduces the amount of tax owing depending on income tax threshold (?)

* Capital gains
  * Tax applies to half the amount if held for > 1 year

* Diversification is not just about holding a range of shares (different industries), but holding bonds (governments), commodities (like gold), property and cash
  * “Hedged” funds offer some protection from currency fluctuations when investing in international funds
    * Worth less when AUD is strong
    * Worth more when AUD is weak
  * “Active” funds means someone is actively buying/selling subsets of an index to try beat the return
    More fees
  * Brokers can hold investments in “trust” or “chess"
    * Trust: held under the broker
    * Chess: held in own name by the broker
  * Robo advisors
    * Automated, algorithmic investment management
    * No control - set & forget based on risk settings
  * Buy / sell spread
    * Estimated transaction costs from buying/selling underlying assets
    * eg brokerage, taxes etc
    * Expressed as a percentage of the funds net asset value (fund size)

## ETF:

  * Stocks over a index fund
    * Priced during the day
    * More control over when transaction applies but depends on buyers/sellers & the number of other transactions in the market
  * Brokerage fees apply for every transaction
    * eg when reinvesting distributions or making monthly contributions
    * SelfWealth $9.5 flat
    * CommSec + Westpac $19.95+ based on amount
  * Distributions paid to you (or can brokers hold it?)
    * Can include different types of income - dividends, capital gains, interest etc
    * Will need to be individually calculated for tax returns (ref)
    * ETF tracking an index rarely passes on Capital Gains as they’re not actively buying/selling
  * Capital gains applies when selling stocks in the ETF
  * Only through a broker
  * Management cost
  * No can’t “charge” people for having shares

## Index funds:

  * “Commencement” fee can be up to 5%
    * Going directly through Vanguard avoids this
  * No fees paid for subsequent transactions
    * eg monthly contributions
  * “Unit” price calculated at the end of each day
    * Transactions are batched
    * 1+ days delay in processing
    * Purchasing via BPay transfer 💩
    * Withdraw via archaic forms 🐌
  * Distributions paid to you then can choose to reinvest or can automatically be reinvested into the same fund
    * Vanguard defaults to reinvest
    * Tax applies regardless of whether it’s paid out or reinvested
  * Capital gains applies for transactions within the fund over the year
    * Divvied up based on no. others investing in the fund?
    * Churn of investors in the fund could cause more transactions to occur & increase the taxable events
  * Management costs
    * Vanguard - fixed cost per year 0.75% pa < 50k
    * Built in
  * Does the trust / chess thing apply for this?

