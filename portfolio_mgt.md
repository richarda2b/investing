# Portfolio Management

## Asset Classes

  * Growth assets
    * Capital growth returns + some distributions
    * Shares, property
    * Potential for higher returns over the long term
  * Income assets
    * Income returns
    * Fixed interest, cash
    * Tend to be more stable but lower returns over the long term
  * Longer timeframe == higher level of growth assets in portfolio
  * Shorter timeframe == higher level of income assets
  * Types of risks:
    * Market
    * Fund - risk that a managed fund will underperform its benchmark or market index due to poor investment selection
    * Regulatory - risk that the fund will be impacted by laws/regulation changes

![](assets/risk_by_asset.png)
![](assets/risk_vs_return.png)

## Rebalancing

  * Asset classes can move from their original allocation within a portfolio - either by under or over performing - impacting the risk level
    * eg shares portion can over perform & represent a higher % of total portfolio
  * Consider the risk of each asset class & costs of selling
  * Subsequent deposits can be used for rebalancing
    * Reduces the need to rebalance through selling
