# Vanguard ETFs as listed in the ASX

| ASX code  | Are                                                |  Fees (p.a) | URL  |
| --------- |:--------------------------------------------------:| -----------:| ---- |
| VAS       | Vanguard Australian Shares Index Fund              |  0.16%      | https://www.vanguard.com.au/adviser/products/en/detail/etf/8205/equity   |
| VGS       | Vanguard International Shares Index Fund           |  0.18%      | https://www.vanguard.com.au/adviser/products/en/detail/etf/8212/equity   |
| VAP       | Vanguard Australian Property Securities Index Fund |  0.23%      | https://www.vanguard.com.au/adviser/products/en/detail/etf/8206/equity   |
| VGE       | Vanguard Emerging Markets Shares Index Fund        |  0.48%      | https://www.vanguard.com.au/adviser/products/en/detail/etf/8204/equity   |
| VGAD      | Vanguard International Shares Index Fund (Hedged)  |  0.21%      | https://www.vanguard.com.au/adviser/products/en/detail/etf/8213/equity   |
| VDHG      | Vanguard High Growth Index Fund                    |  0.27%      | https://www.vanguard.com.au/adviser/products/en/detail/etf/8221/balanced |
